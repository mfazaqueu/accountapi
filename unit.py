import unittest
import json
import sys
sys.path.insert(0, 'src')
from entity.account import Account
from model.account_model import AccountModel
from controller.reset_controller import ResetController
from controller.balance_controller import BalanceController
from controller.event_controller import EventController

class AccountAPITests(unittest.TestCase):
    def setUp(self):
        self.am = AccountModel(None)
        self.rc = ResetController()
        self.bc = BalanceController()
        self.ec = EventController()

        # Cleaning mock db for each test case
        with open('mock_db.json','w') as mockf:
            mock = {"data" : []}
            json.dump(mock, mockf, indent=4)

    def test_create_entity_account_and_get_info(self):
        input_mock = Account("123", 456)
        output_expected = {"id": "123", "balance": 456}        
        self.assertEqual(input_mock.get_info(), output_expected)

    def test_create_entity_account_through_model(self):
        input_mock_id = "123"
        input_mock_balance = 456
        output_expected = {"id": "123", "balance": 456}        
        self.assertEqual(self.am.create(input_mock_id, input_mock_balance).get_info(), output_expected)

    def test_get_existing_account_through_model(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "123", "balance": 456}]}
            json.dump(mock, mockf, indent=4)
        input_mock_id = "123"
        output_expected = {"id": "123", "balance": 456}        
        self.assertEqual(self.am.get(input_mock_id).get_info(), output_expected)

    def test_get_non_existing_account_through_model(self): 
        input_mock_id = "123"
        output_expected = [] 
        self.assertEqual(self.am.get(input_mock_id), output_expected)

    def test_edit_account_through_model(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "123", "balance": 456}]}
            json.dump(mock, mockf, indent=4)
        input_mock_id = "123"
        input_mock_amount = 789
        output_expected = {"id": "123", "balance": 789}        
        self.assertEqual(self.am.edit(input_mock_id, input_mock_amount).get_info(), output_expected)
    
    def test_reset_db_through_model(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "123", "balance": 456}]}
            json.dump(mock, mockf, indent=4)
        output_expected = "OK"       
        self.assertEqual(self.am.reset(), output_expected)

    def test_reset_db_through_controller(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "123", "balance": 456}]}
            json.dump(mock, mockf, indent=4)
        output_expected = {"statusCode": 200, "body": "OK"}
        self.assertEqual(self.rc.reset(), output_expected)

    def test_get_balance_from_existing_account_through_controller(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "123", "balance": 456}]}
            json.dump(mock, mockf, indent=4)
        input_mock_id = "123"
        output_expected = {"statusCode": 200, "body": "456"}
        self.assertEqual(self.bc.get_balance(input_mock_id), output_expected)

    def test_get_balance_from_non_existing_account_through_controller(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "123", "balance": 456}]}
            json.dump(mock, mockf, indent=4)
        input_mock_id = "456"
        output_expected = {"statusCode": 404, "body": 0}
        self.assertEqual(self.bc.get_balance(input_mock_id), output_expected)

    def test_deposit_into_non_existing_account_through_controller(self): 
        input_mock = {"type":"deposit", "destination":"100", "amount":10}
        output_expected = {"statusCode": 201, "body": json.dumps({"destination": {"id":"100", "balance":10}})}
        self.assertEqual(self.ec.operation(input_mock), output_expected)

    def test_deposit_into_existing_account_through_controller(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "100", "balance": 10}]}
            json.dump(mock, mockf, indent=4)
        input_mock = {"type":"deposit", "destination":"100", "amount":10}
        output_expected = {"statusCode": 201, "body": json.dumps({"destination": {"id":"100", "balance":20}})}
        self.assertEqual(self.ec.operation(input_mock), output_expected)

    def test_withdraw_from_existing_account_through_controller(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "100", "balance": 10}]}
            json.dump(mock, mockf, indent=4)
        input_mock = {"type":"withdraw", "origin":"100", "amount":10}
        output_expected = {"statusCode": 201, "body": json.dumps({"origin": {"id":"100", "balance":0}})}
        self.assertEqual(self.ec.operation(input_mock), output_expected)

    def test_withdraw_from_non_existing_account_through_controller(self): 
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "100", "balance": 10}]}
            json.dump(mock, mockf, indent=4)
        input_mock = {"type":"withdraw", "origin":"200", "amount":10}
        output_expected = {"statusCode": 404, "body": "0"}
        self.assertEqual(self.ec.operation(input_mock), output_expected)

    def test_transfer_from_existing_account_through_controller(self):
        with open('mock_db.json','w') as mockf:
            mock = {"data" : [{"id": "100", "balance": 10}]}
            json.dump(mock, mockf, indent=4)
        input_mock = {"type":"transfer", "origin":"100", "amount":10, "destination":"300"}
        output_expected = {"statusCode": 201, "body": json.dumps({"origin": {"id":"100", "balance":0}, "destination": {"id":"300", "balance":10}})}
        self.assertEqual(self.ec.operation(input_mock), output_expected)

    def test_transfer_from_non_existing_account_through_controller(self):
        input_mock = {"type":"transfer", "origin":"200", "amount":15, "destination":"300"}
        output_expected = {"statusCode": 404, "body": "0"}
        self.assertEqual(self.ec.operation(input_mock), output_expected)

