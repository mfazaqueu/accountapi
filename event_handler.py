import json
import sys
sys.path.insert(0, '/opt')
from controller.event_controller import EventController

def handler(event, context):
    try:
        EC = EventController()
        response = EC.operation(json.loads(event["body"]))
        return response
    except Exception as e:
        print(str(e))
        return {
            "statusCode": 500,
            "headers": {"Content-Type": "application/json; charset=utf-8", "Access-Control-Allow-Origin" : "*"},
            "body": 0
        }

