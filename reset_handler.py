import json
import sys
sys.path.insert(0, '/opt')
from controller.reset_controller import ResetController

def handler(event, context):
    try:
        RC = ResetController()
        return RC.reset()
    except Exception as e:
        print(str(e))
        return {
            "statusCode": 500,
            "headers": {"Content-Type": "application/json; charset=utf-8", "Access-Control-Allow-Origin" : "*"},
            "body": 0
        }
