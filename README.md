### Account API project architecture

<div align="center">
  <img src="doc/diagram.png">
</div>

### Requirements for local env
| Plugin | README |
| ------ | ------ |
| Serverless Framework | https://github.com/serverless/serverless |
| Python 3.8 | https://docs.python.org/3.8/ |
| Unittest | https://docs.python.org/3/library/unittest.html |
| AWS account | https://docs.aws.amazon.com/index.html |

* Windows only


### Running unit tests

```sh
$ cd accountapi
$ python -m unittest unit.py
```

### Setting up AWS account into Serverless project

```sh
$ cd accountapi
$ serverless config credentials --provider aws --key {YOUR-ACCESS-KEY} --secret {YOUR-SECRET-KEY}
```

### Deploying project to an AWS account

```sh
$ cd accountapi
$ serverless deploy
```

### Todos

 - TBD

