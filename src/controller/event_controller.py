import json
from model.account_model import AccountModel
from entity.account import Account
import sys
if sys.platform != "win32" and sys.platform != "win64":
    import boto3

class EventController:
    def __init__(self):
        if 'boto3' in globals():
            self.account_model = AccountModel(boto3.resource('dynamodb').Table('AccountTable'))
        else:
            self.account_model = AccountModel(None)

    def operation(self, data):
        try:
            if data['type'] == "deposit":
                return self.deposit(data)                        
            elif data['type'] == "withdraw":
                return self.withdraw(data) 
            elif data['type'] == "transfer":
                return self.transfer(data)
            else:
                return {
                    "statusCode": 405,
                    "body": "0"
                }
        except Exception as e:           
            return {
                    "statusCode": 500,
                    "body": "0"
                }

    def deposit(self, data):
        try:
            destination = self.account_model.get(data['destination'])
            if destination != []:
                destination = self.account_model.edit(data['destination'], destination['balance'] + data['amount']) 
            else: 
                destination = self.account_model.create(data['destination'], data['amount'])
            return {
                "statusCode": 201,
                "body": json.dumps({
                    "destination": destination.get_info()
                })
            }
        except Exception as e:
            raise Exception(str(e))

    def withdraw(self, data):
        try:
            origin = self.account_model.get(data['origin'])
            if origin != []:
                origin = self.account_model.edit(data['origin'], origin['balance'] - data['amount']) 
                return {
                    "statusCode": 201,
                    "body": json.dumps({
                        "origin": origin.get_info()
                    })
                }
            else:
                return {
                    "statusCode": 404,
                    "body": "0"
                }
        except Exception as e:
            raise Exception(str(e))

    def transfer(self, data):
        try:
            origin = self.account_model.get(data['origin'])
            destination = self.account_model.get(data['destination'])
            if origin != []:
                origin = self.account_model.edit(data['origin'], origin['balance'] - data['amount'])                 
                if destination != []:
                    destination = self.account_model.edit(data['destination'], destination['balance'] + data['amount'])
                else:
                    destination = self.account_model.create(data['destination'], data['amount'])
                
                return {
                    "statusCode": 201,
                    "body": json.dumps({
                        "origin": origin.get_info(),
                        "destination": destination.get_info()
                    })
                }
            else:
                return {
                    "statusCode": 404,
                    "body": "0"
                }
        except Exception as e:
            raise Exception(str(e))