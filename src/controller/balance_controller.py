import json
from model.account_model import AccountModel
from entity.account import Account
import sys
if sys.platform != "win32" and sys.platform != "win64":
    import boto3

class BalanceController:
    def __init__(self):
        if 'boto3' in globals():
            self.account_model = AccountModel(boto3.resource('dynamodb').Table('AccountTable'))
        else:
            self.account_model = AccountModel(None)

    def get_balance(self, account_id):
        try:
            account = self.account_model.get(str(account_id))
            if account != []:
                return {
                    "statusCode": 200,
                    "body": json.dumps(account.get_info()['balance'])
                }
            else:
                return {
                    "statusCode": 404,
                    "body": 0
                }
        except Exception as e:
            return {
                    "statusCode": 500,
                    "body": str(e)
                }