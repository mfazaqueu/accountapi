import json
from model.account_model import AccountModel
import sys
if sys.platform != "win32" and sys.platform != "win64":
    import boto3

class ResetController:
    def __init__(self):        
        if 'boto3' in globals():
            self.account_model = AccountModel(boto3.resource('dynamodb').Table('AccountTable'))
        else:
            self.account_model = AccountModel(None)

    def reset(self):
        try:
            if self.account_model.reset() == "OK":
                return {
                    "statusCode": 200,
                    "body": "OK"
                }
            else:
                return {
                    "statusCode": 500,
                    "body": 0
                }
        except Exception as e:
            return {
                    "statusCode": 500,
                    "body": 0
                }