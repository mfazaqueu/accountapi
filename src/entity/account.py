import json

class Account:
    def __init__(self, account_id, balance):
        self.id = account_id
        self.balance = balance

    def __getitem__(self, index):
        return getattr(self, index)

    def get_info(self):
        return {
            "id": self.id,
            "balance": self.balance
        }