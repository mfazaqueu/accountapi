import json
from entity.account import Account
import sys
if sys.platform != "win32" and sys.platform != "win64":
    import boto3
    from boto3.dynamodb.conditions import Key
    from decimal import Decimal

class AccountModel:
    def __init__(self, db):
        self.db = db
        if self.db == None:
            self.mockfile = 'mock_db.json'

    def get(self, account_id):        
        try:
            if self.db != None:  
                response = self.db.query(
                    KeyConditionExpression=Key("id").eq(account_id)
                )
                if len(response['Items']) > 0:
                    return Account(response['Items'][0]['id'], int(response['Items'][0]['balance']))
                return []
            else:
                with open(self.mockfile,'r') as mockf:
                    mock = json.load(mockf)
                    for account in mock["data"]:                              
                        if account['id'] == account_id:
                            return Account(account_id, account['balance'])
                    return []
        except Exception as e:
            raise Exception(e)
        
    def create(self, account_id, amount):
        try:
            if self.db != None:
                response = self.db.put_item(
                    Item={
                            'id': account_id,
                            'balance': Decimal(amount)
                        },
                )
                return Account(account_id, amount)
            else:         
                with open(self.mockfile,'r') as mockf:
                    mock = json.load(mockf)
                    mock["data"].append({"id": account_id, "balance": amount})

                    with open(self.mockfile,'w') as mockf:
                        json.dump(mock, mockf, indent=4)

                return Account(account_id, amount)
        except Exception as e:
            raise Exception(e)

    def edit(self, account_id, amount):
        try:
            if self.db != None:
                response = self.db.update_item(
                    Key={
                        'id': account_id
                    },
                    UpdateExpression="set balance = :b",
                    ExpressionAttributeValues={
                        ':b': Decimal(amount)
                    },
                    ReturnValues="UPDATED_NEW"
                )
                return Account(account_id, int(response['Attributes']['balance']))
            else:         
                with open(self.mockfile,'r') as mockf:
                    mock = json.load(mockf)
                    for acc in range(len(mock["data"])):
                        if mock["data"][acc]["id"] == account_id:
                            mock["data"][acc]["balance"] = amount

                    with open(self.mockfile,'w') as mockf:
                        json.dump(mock, mockf, indent=4)
                        
                return Account(account_id, amount)
        except Exception as e:
            raise Exception(e)
    
    def reset(self):
        try:                 
            if self.db != None:
                scan = self.db.scan()
                with self.db.batch_writer() as batch:
                    for each in scan['Items']:
                        batch.delete_item(
                            Key={
                                'id': each['id'],
                            }
                )
                return "OK"
            else:         
                with open(self.mockfile,'w') as mockf:
                    mock = {"data" : []}
                    json.dump(mock, mockf, indent=4)
                return "OK"
        except Exception as e:
            raise Exception(e)