import json
import sys
sys.path.insert(0, '/opt')
from controller.balance_controller import BalanceController

def handler(event, context):
    try:
        account_id = event['queryStringParameters']['account_id']
        BC = BalanceController()
        response = BC.get_balance(int(account_id))
        return response
    except Exception as e:
        print(e)
        return {
            "statusCode": 500,
            "body": "0"
        }
    
